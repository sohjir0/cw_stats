# Clan war stats

Para saber a quien botar

## Configuraciones y scripts
Para correr la aplicación de una manera sencilla puedes ejecutarlo de la siguiente manera

```sh
> npm start
```

Si deseas correrlo en modo producción o algún otro ambiente puedes hacerlo exportando la variable de entorno `NODE_ENV=ambiente`

```sh
> export NODE_ENV=production
> npm start
```

De esta manera buscará el archivo de configuración dentro de la carpeta config.

### Livereload

Si deseas realizar cambios en modo desarrollo y no tener que estar reiniciando la aplicación puedes realizarlo de la siguiente manera:

```sh
> npm run live
```
