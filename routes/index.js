var express = require('express');
var router = express.Router();
var date_formatter = require('../helpers/date_formatter.js');
const assert = require('assert');

var db = require('../db.js');
var WarLog = require('../models/war_log.js');

router.get('/', function(req, res, next) {
  getClanMembers().then(clanMembers => {
    Promise.all(clanStatistics(clanMembers)).then(statistics => {
      statistics.sort((a, b) => (a.winRatio - b.winRatio) * -1);

      res.render('index', {
        title: 'Clan',
        message: 'Clan wars',
        statistics: statistics,
      });
    });
  });
});

var clanStatistics = function(clanMembers) {
  return clanMembers.map(clanMember => {
    return new Promise(function(resolve, reject) {
      getStatistics(clanMember.name).then(warData => {
        resolve(new WarLog(clanMember.name, warData));
      });
    });
  });
};


function getClanMembers() {
  return db.get().collection("members").find({}).sort({tag: -1}).toArray();
}

function getStatistics(name) {
  return db.get().collection("statistics").find({name: name}).toArray();
}

module.exports = router;
