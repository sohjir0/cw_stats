const Statistic = function(war_id, participant) {
  this._id = `${war_id}-${participant.tag}`
  this.war = war_id
  this.tag = participant.tag;
  this.name = participant.name;
  this.battlesPlayed = participant.battlesPlayed;
  this.wins = participant.wins;
};

module.exports = Statistic;
