var Arena = function(data) {
  this.name = data.name;
  this.arena = data.arena;
  this.arenaID = data.arenaID;
  this.trophyLimit = data.trophyLimit;
}

module.exports = Arena;
