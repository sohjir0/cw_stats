var Arena = require('./arena.js');

var Member = function(member) {
  this._id = member.tag
  this.name = member.name;
  this.tag = member.tag;
  this.rank = member.rank;
  this.previousRank = member.previousRank;
  this.role = member.role;
  this.expLevel = member.expLevel;
  this.trophies = member.trophies;
  this.clanChestCrowns = member.clanChestCrowns;
  this.donations = member.donations;
  this.donationsReceived = member.donationsReceived;
  this.donationsDelta = member.donationsDelta;
  this.donationsPercent = member.donationsPercent;
  this.arena = new Arena(member.arena);
}

module.exports = Member;
