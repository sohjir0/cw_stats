var db = require('../db.js')
var config = require("config");
var client = require('./client.js');
var Member = require('../models/member.js');

exports.get = function() {
  client.get(config.api.clan_url, clanDataRequestSuccess);
}

function clanDataRequestSuccess(clan) {
  clan.members.forEach(function(memberRaw) {
    var member = new Member(memberRaw);
    db.get().collection('members').insertOne(member, function(err, result) {
      // TODO: Ignoramos el error de llave duplicada de esta manera evitamos preguntar
      // si el registro se encuentra agregado
    });
  })
};

setInterval(this.get, 3600000);
