const assert = require('assert');
var request = require('request');
var config = require("config");

const options = {'auth': { 'bearer': config.crapi.token } };

exports.get = function(url, success) {
  request.get(url, options, function(error, response, body) {
    if (response.statusCode == 200) {
      success(JSON.parse(body));
    }
  });
}
