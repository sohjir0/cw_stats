var db = require('../db.js')
var config = require("config");
var Statistic = require('../models/statistic.js');
var client = require('./client.js');

exports.get = function() {
  client.get(config.api.clan_warlog_url, clanWarRequestSuccess);
};

function clanWarRequestSuccess(wars) {
  wars.forEach(function(war) {
    war.participants.forEach(function(participant) {
      var statistic = new Statistic(war.createdDate, participant);
      db.get().collection('statistics').insertOne(statistic, function(err, result) {
        // TODO: Ignoramos el error de llave duplicada de esta manera evitamos preguntar
        // si el registro se encuentra agregado
      });
    });
  });
}

setInterval(this.get, 3600000);
